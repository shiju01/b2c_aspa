from frappe import _

def get_data():
	return [
		{
			"label": _("Call Module"),
			"icon": "icon-star",
			"items": [
				{
					"type": "doctype",
					"name": "ASPA Call",
					"label": "Calls",
					"description": _("List of Call Logs. Begin here."),
				},
			]
		},
		{
			"label": _("Masters"),
			"icon": "icon-star",
			"items": [
				{
					"type": "doctype",
					"name": "ASPA Machine",
					"label": "Machines",
					"description": _("List of Machines"),
				},
				{
					"type": "doctype",
					"name": "ASPA Machine Agreement",
					"label": "Machine Agreements",
					"description": _("List of Machine Agreements"),
				},
				{
					"type": "doctype",
					"name": "ASPA Toner Request",
					"label": "Toner Request",
					"description": _("List of Toner Requests"),
				},
				{
					"type": "doctype",
					"name": "ASPA Machine Family",
					"label": "Machine Families",
					"description": _("List of Machine Families"),
				},
				{
					"type": "doctype",
					"name": "ASPA Meter Reader",
					"label": "ASPA Meter Reader",
					"description": _("List of Patch Meter Readers"),
				},
				{
					"type": "doctype",
					"name": "ASPA Patch Meter",
					"label": "Patch Meters",
					"description": _("List of Patch Meters"),
				},
			]
		},
		{
			"label": _("Tools"),
			"icon": "icon-star",
			"items": [
				{
					"type": "doctype",
					"name": "ASPA Monthly Billing Tool",
					"label": "FSMA Monthly Billing Tool",
					"description": _("Tool for monthly FSMA billing."),
				},
				{
					"type": "page",
					"name": "aspa-monthly-billing",
					"label": "FSMA Monthly Billing Tool 2",
					"description": _("Tool for monthly FSMA billing."),
				},
				{
					"type": "doctype",
					"name": "ASPA Machine Meter Recording Tool",
					"label": "Patch Meter Reading Tool",
					"description": _("Tool for recording patch meter readings."),
				},
			]
		},
		{
			"label": _("Dev"),
			"icon": "icon-star",
			"items": [
				{
					"type": "doctype",
					"name": "ASPA Meter Reading",
					"label": "ASPA Meter Reading",
					"description": _("Machine Meter Reading"),
				},
				{
					"type": "doctype",
					"name": "ASPA Call Visit",
					"label": "Visits",
					"description": _("List of all logged visits."),
				},
				{
					"type": "doctype",
					"name": "ASPA Call Allocation",
					"label": "Allocations",
					"description": _("List of all allocations."),
				},
			]
		},
	]