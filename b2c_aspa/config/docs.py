"""
Configuration for docs
"""

# source_link = "https://github.com/[org_name]/b2c_aspa"
# docs_base_url = "https://[org_name].github.io/b2c_aspa"
# headline = "App that does everything"
# sub_heading = "Yes, you got that right the first time, everything"

def get_context(context):
	context.brand_html = "B2C ASPA"
