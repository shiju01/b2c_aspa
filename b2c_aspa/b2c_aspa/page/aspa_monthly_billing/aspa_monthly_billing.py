from __future__ import unicode_literals
import frappe
import datetime
from frappe import _
from frappe.model.document import Document
import json
from b2c_aspa.api import add_meter_reading
from frappe.geo.doctype.address.address import get_address_display

@frappe.whitelist()
def get_monthly_records(patch_meter_reader, month):
	#ACTIVATE THIS
	refdate = frappe.utils.datetime.datetime.strptime("01 {0} 2017".format(month), "%d %B %Y")

	#refdate = frappe.utils.datetime.datetime(2017,04,1)

	first_of_lastmonth = frappe.utils.add_months(refdate, -1).replace(day=1)
	last_of_thismonth = frappe.utils.add_days(frappe.utils.add_months(refdate.replace(day=1), 1), -1)

	# print first_of_lastmonth, last_of_thismonth
	# print "READER", patch_meter_reader
	# print "Machines for reader", machines_for_reader

	machines_for_reader = frappe.get_all("ASPA Machine", filters=[["meter_reader", "=", patch_meter_reader]], fields=["*"])

	all_meters = frappe.get_all("ASPA Patch Meter")

	meters_for_all_machines = frappe.get_all("ASPA Machine Meter", fields=["*"])

	readings = frappe.db.sql("""SELECT machine, reading_time, meter_reader, reading, meter FROM `tabASPA Meter Reading`
		WHERE reading_type='Patch'
		AND DATE(reading_time) BETWEEN '{startdate}' AND '{enddate}' ORDER BY meter, reading_time;""".format(startdate=first_of_lastmonth, enddate=last_of_thismonth),
	as_dict=1)

	machines_info = []
	for machine in machines_for_reader:
		machine_meters = [m.patch_meter for m in meters_for_all_machines if m.parent == machine.name]
		machine_meter_readings = [r for r in readings if r.machine == machine.name and r.meter in machine_meters]

		readings_by_meter = []
		for meter in machine_meters:
			previous_reading_list = [{"reading": r.reading, "reading_time": r.reading_time} for r in machine_meter_readings if
								r.meter == meter and
								(r.reading_time.year == first_of_lastmonth.year and r.reading_time.month == first_of_lastmonth.month)]
			previous_reading = previous_reading_list[0] if len(previous_reading_list) > 0 else {"reading": 0, "reading_time": None}

			current_reading_list = [{"reading": r.reading, "reading_time": r.reading_time} for r in machine_meter_readings if
								r.meter == meter and
								(r.reading_time.year == last_of_thismonth.year and r.reading_time.month == last_of_thismonth.month)]
			current_reading = current_reading_list[0] if len(current_reading_list) > 0 else {"reading": 0, "reading_time": None}

			# print "PREV", previous_reading
			# print "CURR", current_reading

			reading_by_meter = {
				"meter": meter,
				"previous_reading": previous_reading.get("reading"),
				"previous_reading_time": previous_reading.get("reading_time"),
				"current_reading": current_reading.get("reading"),
				"current_reading_time": current_reading.get("reading_time")
			}
			readings_by_meter.append(reading_by_meter)

			#print "test", readings_by_meter
		machine_row = frappe._dict({
			"mi": machine,
			"meters": machine_meters,
			"readings_by_meter" : readings_by_meter
		})

		machines_info.append(machine_row)

	return { "all_meters": all_meters, "machines_info": machines_info }

@frappe.whitelist()
def generate_fsma_invoices(month, patch_meter_reader):
	print "MONTH", month
	sales_invoice = frappe.db.sql("""SELECT posting_date FROM `tabSales Invoice` where EXTRACT(MONTH FROM posting_date) = '{month}'""".format(month=frappe.utils.datetime.datetime.strptime('{0}'.format(month),"%B").month),as_dict=1)

	abbr = frappe.db.get_value("Company", frappe.defaults.get_defaults().company, "abbr")

	refdate = frappe.utils.datetime.datetime.strptime("01 {0} 2017".format(month), "%d %B %Y")

	for x in xrange(1,10):
		print "refdate",unicode(refdate)
	first_of_lastmonth = frappe.utils.add_months(refdate, -1).replace(day=1)
	last_of_thismonth = frappe.utils.add_days(frappe.utils.add_months(refdate.replace(day=1), 1), -1)

	# first_of_lastmonth = frappe.utils.add_months(frappe.utils.datetime.date.today(), -1).replace(day=1)
	# last_of_thismonth = frappe.utils.add_days(frappe.utils.add_months(frappe.utils.datetime.date.today().replace(day=1), 1), -1)

	print first_of_lastmonth
	print last_of_thismonth

	machines = frappe.get_all("ASPA Machine", [["meter_reader", "=", patch_meter_reader]])
	count = 1

	messages = []

	for machine in machines:
		#Results are sorted. Can look up row 0 and 1.
		current_last_readings = frappe.db.sql("""SELECT machine, reading_time, meter_reader, reading, meter FROM `tabASPA Meter Reading`
			WHERE reading_type='Patch'
			AND machine='{machine}' AND meter_reader='{patch_meter_reader}' 
			AND DATE(reading_time) BETWEEN '{startdate}' AND '{enddate}' ORDER BY meter, reading_time DESC;""".format(machine=machine.name, startdate=first_of_lastmonth, enddate=last_of_thismonth, patch_meter_reader=patch_meter_reader),
		as_dict=1)
		if not len(sales_invoice) > 0:
			si = frappe.new_doc("Sales Invoice")
			si.transaction_date = frappe.utils.get_datetime()
			si.due_date = frappe.utils.add_days(frappe.utils.get_datetime(), 10) #Might be decided via agreement.
			si.price_list = "Standard Selling"
			si.customer = frappe.db.get_value("ASPA Machine", machine.name, "customer")
			filters = [ 
			    ["Dynamic Link", "link_doctype", "=", "Customer"],
			    ["Dynamic Link", "link_name", "=", si.customer],
			    ["Dynamic Link", "parenttype", "=", "Contact"],
			    ["is_primary_contact", "=", 1]]
			contact_list = frappe.get_all("Contact", filters=filters, fields=["*"])
			si.customer_tel_no = contact_list[0].phone
			
			address_dict = frappe.db.get_value("ASPA Machine", machine.name, "installation_address")
			si.customer_address = address_dict
			
			si.aspa_customer_id = frappe.db.get_value("Customer", si.customer, "aspa_customer_id")
			si.aspa_machine = machine.name
			si.aspa_machine_model_no = frappe.db.get_value("ASPA Machine", machine.name, "model")
			si.aspa_machine_agreement = frappe.db.get_value("ASPA Machine", machine.name, "active_agreement")
			si.aspa_machine_agreement_start_date = frappe.db.get_value("ASPA Machine Agreement", si.aspa_machine_agreement, "agreement_start_date")

			machine_meters_list = frappe.get_all("ASPA Machine Meter", filters={"parent": machine.name}, fields=["patch_meter"])
			machine_meters = [m.patch_meter for m in machine_meters_list]

			print "Machine", machine
			for meter in machine_meters:
				print meter

				readings_for_meter = [r for r in current_last_readings if r.meter == meter]
				# print "Current, last readings", current_last_readings
				print "Readings for meter", readings_for_meter

				# if len(readings_for_meter) != 2:
				# 	frappe.throw("FSMA Invoicing: Some readings missing! Meter: {0}, Machine: {1}".format(meter, machine.name))

				current_meter_reading = readings_for_meter[0].reading
				last_meter_reading = readings_for_meter[1].reading


				difference = last_meter_reading - current_meter_reading
				adjusted_difference = int(difference * 0.99)

				print "LMR", last_meter_reading, "CMR", current_meter_reading
				print "Diff", difference
				print "Adj Diff", adjusted_difference

				item_description = "<div><table> <tr> <td>Meter </td><td>{meter}</td> </tr> <tr> <td>Last Meter Reading</td> <td>{last_meter_reading}</td> </tr> <tr> <td>Current Meter Reading</td> <td>{current_meter_reading}</td> </tr> <tr> <td>Difference</td> <td>{difference}</td> </tr> <tr> <td>Adjusted (x0.99)</td> <td>{adjusted_difference}</td> </tr> </table> </div>".format(meter = meter, last_meter_reading=last_meter_reading or 0.0, current_meter_reading=current_meter_reading or 0.0, difference=difference or 0.0, adjusted_difference=adjusted_difference or 0.0)

				si.append("items", {
					"item_name": meter,
					"uom": "Nos",
					"description": item_description,
					"qty": adjusted_difference,
					"rate": 100.0, #Get charges at per copy print
					"amount": adjusted_difference * 100.0,
					"conversion_factor": 1,
					"income_account": "Sales - " + abbr
				})
				count = count + 1
			try:
				si.save()
				frappe.db.commit()
			except Exception as e:
				messages.append("Error: Machine:{0} <br> {2}".format(machine.name, month, e.message))
			else:
				messages.append("Success: Machine Sr/No {0}".format(machine.name, month))
			finally:
				pass

		else:
			frappe.throw(_("Invoices already generated for {0}- 2017:  {1}. New invoices: {1}").format(month, count))

	return "<br/>".join(messages)

@frappe.whitelist()
def save_monthly_readings(patch_meter_reader, month, monthly_readings):

	refdate = frappe.utils.datetime.datetime.strptime("01 {0} 2017".format(month), "%d %B %Y")

	previous_readings = get_monthly_records(patch_meter_reader, month)
	previous_readings = previous_readings["machines_info"]

	# print "CURRENT READINGS: ", monthly_readings
	print "PREVIOUS READINGS: ", previous_readings

	if monthly_readings:
		monthly_readings = json.loads(monthly_readings)

	for monthly_reading in monthly_readings:
		for current_reading in monthly_reading.get("current_readings"):
			print "Machine: ", monthly_reading.get("machine")
			print "Meter: ", current_reading.get("meter"), "Reading:", current_reading.get("reading")



			# existing_readings_for_month = [pr.get("readings_by_meter") for pr in previous_readings
			# 	if pr.get("mi").get("serial_no") == monthly_reading.get("machine")
			# 	and (len([rbm for rbm in pr.get("readings_by_meter") if
			# 		frappe.utils.getdate(rbm.get("reading_time")).month == refdate.month
			# 		and frappe.utils.getdate(rbm.get("reading_time")).year == refdate.year]) >0)
			# ]

			#print existing_readings

			# if len([er for er in existing_readings if er.get("meter") == current_reading.get("meter") and er.get("reading") > 0]):

			# 	print "Existing readings:", existing_readings

			add_meter_reading(monthly_reading.get("machine"),
				"Patch",
				datetime.datetime.combine(refdate, frappe.utils.get_time(frappe.utils.get_datetime())),
				current_reading.get("reading"),
				current_reading.get("meter"),
			 	patch_meter_reader)
