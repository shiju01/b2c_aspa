frappe.pages['aspa-monthly-billing'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: __('Monthly Billing Tool'),
		single_column: true
	});

	frappe.billingtool = new frappe.ASPAFSMABillingTool(page);
}


frappe.ASPAFSMABillingTool = Class.extend({
	init: function(page) {
		this.page = page;
		this.wrapper = page.wrapper.find('.page-content');
		this.make();
	},

	set_route_options: function() {
		//??
	},
	make: function() {
		var me = this;
		this.page.add_inner_button(__("Save"), function() {
			var rows = $("tr[data-purpose='machine-data-row']");

			var machine_readings = [];

			$.each(rows, function(rowindex, row) {
				var machine_reading = {};

				machine_reading["machine"] = $(row).attr("id");

				var inputs = $(row).find("[data-purpose='reading-input']");
				var readings_by_meter = [];
				$.each(inputs, function(index, textbox) {
					reading_by_meter = {
						"meter": $(textbox).attr('data-meter'),
						"reading": $(textbox).val()
					};
					readings_by_meter.push(reading_by_meter);
				});
				machine_reading["current_readings"] = readings_by_meter;
				machine_readings.push(machine_reading);
			});

			console.log("Machine readings", machine_readings);

			frappe.call({
				method: "b2c_aspa.b2c_aspa.page.aspa_monthly_billing.aspa_monthly_billing.save_monthly_readings",
				args: {
					"patch_meter_reader": me.page.fields_dict['patch_meter_reader'].$input.val(),
					"month": me.page.fields_dict['month'].$input.val(),
					"monthly_readings": machine_readings
				},
				callback: function(r) {
					if (r && r.message) {
						frappe.msgprint(r.message, "Readings for " + me.page.fields_dict['month'].$input.val());
					}
				}
			});
		});

		this.page.add_inner_button(__("Generate Proforma"), function() {
			console.log("Generate Proforma")
		});

		this.page.add_inner_button(__("Generate FSMA Invoices"), function() {
			frappe.call({
				method: "b2c_aspa.b2c_aspa.page.aspa_monthly_billing.aspa_monthly_billing.generate_fsma_invoices",
				args: {
					"month": me.page.fields_dict['month'].$input.val(),
					"patch_meter_reader": me.page.fields_dict['patch_meter_reader'].$input.val()
				},
				callback: function(r) {
					console.log(r);
					if (r && r.message) {
						frappe.msgprint(r.message, "FSMA Invoices for " + me.page.fields_dict['month'].$input.val());
					}
				}
			})
		});

		this.page.add_field(
			{
				fieldtype: "Link",
				fieldname: "patch_meter_reader",
				options: "ASPA Meter Reader",
				label: __("Patch Meter Reader"),
				input_css: {"z-index": 1},
				change: function(event) {
					me.get_data_and_render();
				},
			}
		)
		this.page.add_field(
			{
				fieldtype: "Select",
				fieldname: "month",
				options: moment.months().join("\n"),
				label: __("Month"),
				default: moment().format("MMMM"),
				input_css: {"z-index": 1},
				change: function() {
					me.get_data_and_render();
				},
			}
		);
		this.get_data_and_render();
	},
	get_data_and_render: function() {
		var pageref = this.page;

		if (pageref.fields_dict['patch_meter_reader'].$input.val() &&  pageref.fields_dict['month'].$input.val()) {
			frappe.call({
				method: "b2c_aspa.b2c_aspa.page.aspa_monthly_billing.aspa_monthly_billing.get_monthly_records",
				args: {
					patch_meter_reader: this.page.fields_dict['patch_meter_reader'].$input.val(),
					month: this.page.fields_dict['month'].$input.val()
				},
				callback: function(r){
					pageref.wrapper.find("#monthly-fsma-readings").remove();
					pageref.main.after(frappe.render_template("monthly_billing_tool_content", {
							"all_meters": r.message.all_meters,
							"machines_info": r.message.machines_info
						}
					));
				}
			})
		}
	}
});
