// Copyright (c) 2016, MN Technique and contributors
// For license information, please see license.txt

frappe.ui.form.on('ASPA Machine Meter Recording Tool', {
	refresh: function(frm) {
		frm.disable_save();
		frm.page.page_actions.remove();
		render_readings(frm);
	},
	machine: function(frm){
		if(frm.doc.machine){
			render_readings(frm);
			set_last_reading(frm);
		} else {
			cur_frm.set_value("meter_reading", 0);
		}
	},
	update_reading: function(frm){
		var frm = frm;
		frappe.call({
			"doc": frm.doc,
			"method": "update_reading",
			callback: function(r){
				render_readings(frm);
			}
		});	
	}
});

function render_readings(frm){
	var render_field = $(frm.fields_dict["machine_reading_history_html"].wrapper);
	var rendered_html = "";
	frappe.call({
		"doc": frm.doc,
		"method": "render_readings",
		callback: function(r){
			if (r){
				rendered_html = frappe.render_template("meter_reading_history", {
					"reading_history": r.message,
					"machine": cur_frm.doc.machine
				});
				render_field.html(rendered_html);
				render_field.find(".view-all").on("click", function(event) {
					frappe.route_options = {"machine": cur_frm.doc.machine}
					frappe.set_route("List", "ASPA Patch Meter Reading");
				});
			}
		}
	});
}

function set_last_reading(frm){
	frappe.call({
		"method": "b2c_aspa.b2c_aspa.doctype.aspa_machine_meter_recording_tool.aspa_machine_meter_recording_tool.get_last_reading",
		"args": {"machine":frm.doc.machine},
		callback: function(r){
			if (r){
				console.log(r);
				cur_frm.set_value("meter_reading", r.message)
			}
		}
	});
}