# -*- coding: utf-8 -*-
# Copyright (c) 2015, MN Technique and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class ASPAMachineMeterRecordingTool(Document):
	def render_readings(self):
		readings = frappe.get_all("ASPA Patch Meter Reading", 
						filters={
							"machine":self.machine
						}, 
						fields=["*"])
		return readings

	def update_reading(self):
		pmr = frappe.new_doc("ASPA Patch Meter Reading")
		pmr.machine = self.machine
		pmr.employee = self.employee
		pmr.reading_time = frappe.utils.datetime.datetime.now()
		pmr.reading = self.meter_reading
		pmr.save(ignore_permissions=True)
		frappe.db.commit()

@frappe.whitelist()
def get_last_reading(machine):
	datetimes = frappe.get_all("ASPA Patch Meter Reading", filters={"machine":machine}, fields=["*"])
	now = frappe.utils.datetime.datetime.now()
	last_reading = 0
	if len(datetimes) > 0:
		last_reading = max([dt.reading for dt in datetimes if dt.reading_time < now])
	return last_reading