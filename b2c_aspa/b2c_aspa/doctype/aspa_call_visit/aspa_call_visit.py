# -*- coding: utf-8 -*-
# Copyright (c) 2015, MN Technique and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from b2c_aspa.api import add_meter_reading

class ASPACallVisit(Document):
	def validate(self):
		self.has_drafts()

	def on_submit(self):
		if self.call_status_after_visit not in ["Broken", "Closed"]:
			frappe.throw("Please Close or Break the call before submitting.")

		add_meter_reading(self.machine, "Visit", frappe.utils.get_datetime(), self.meter_reading, "A4BW", frappe.session.user)

		#allocated_call.save()
		frappe.db.set_value("ASPA Call", self.allocated_call, "workflow_state", self.call_status_after_visit)
		frappe.db.commit()

	def has_drafts(self):
		visits = frappe.get_all(self.doctype, filters=[["docstatus", "=", 0], ["allocated_call", "=", self.allocated_call], ["name", "!=", self.name]])
		if len(visits) > 0:
			frappe.throw("Please submit existing visits for this call. <br> Unsubmitted visit(s): <br>{0}".format("\n".join([v.name for v in visits])))

	def get_meters_and_readings(self):
		refdate = frappe.utils.datetime.datetime.now()

		meters_for_all_machines = frappe.get_all("ASPA Machine Meter", fields=["*"])
		first_of_lastmonth = frappe.utils.add_months(refdate, -1).replace(day=1)
		last_of_thismonth = frappe.utils.add_days(frappe.utils.add_months(refdate.replace(day=1), 1), -1)

		readings = frappe.db.sql("""SELECT A.machine, A.reading_time, A.meter_reader, A.reading, A.meter
			FROM `tabASPA Meter Reading` AS A inner join `tabDynamic Link` AS B on
			A.name=B.parent WHERE machine = '{machine}' AND B.link_name = '{call_name}' ORDER BY meter, reading_time;"""
			.format(machine=self.machine, call_name= self.name),
		as_dict=1)
		# print "readings", readings
		machine_meters = [m.patch_meter for m in meters_for_all_machines if m.parent == self.machine]
		machine_meter_readings = [r for r in readings if r.machine == self.machine and r.meter in machine_meters]

		readings_by_meter = []
		for meter in machine_meters:

			previous_reading_list = [r.reading for r in machine_meter_readings if
								r.meter == meter and
								(r.reading_time.year == first_of_lastmonth.year and r.reading_time.month == first_of_lastmonth.month)]
			previous_reading = previous_reading_list[0] if len(previous_reading_list) > 0 else 0

			reading_list = [r.reading for r in machine_meter_readings if
								r.meter == meter]
			reading = reading_list[0] if len(reading_list) > 0 else 0
			print "machine_meter", reading
			reading_by_meter = {
				"meter": meter,
				"previous_reading": reading,
				"current_reading": 0
			}
			readings_by_meter.append(reading_by_meter)
			# print "test2", readings_by_meter
		return readings_by_meter