# -*- coding: utf-8 -*-
# Copyright (c) 2015, MN Technique and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import erpnext

class ASPATonerRequest(Document):
	#TODO: Add validation to approve toner request.
	def validate(self):
		self.check_duplicates()

	def after_insert(self):
		self.submit() #force submit.

	def on_submit(self):
		frappe.db.set_value("ASPA Call", self.call, "toner_request", self.name)
		frappe.publish_realtime(event="eval_js", message="cur_frm.refresh()", doctype="ASPA Call", docname=self.call)

		#Create stock entry
		self.make_new_stock_entry()


	def before_cancel(self):
		self.clear_linked_doctypes()	

	def check_duplicates(self):
		siblings = frappe.get_all("ASPA Toner Request", filters=[["call", "=", self.call],["name", "!=", self.name]])
		
		if len(siblings) > 0:
			frappe.throw("Toner Request {0} already created against this call".format(siblings[0].name))
	
	def clear_linked_doctypes(self):
		frappe.db.set_value(self.doctype, self.name, "call", "")
		#frappe.db.commit()
	
	def make_new_stock_entry(self):	
		if not self.toner_stock_entry: #and self.to_warehouse != wh_loss:
			s = frappe.new_doc("Stock Entry")
			s.posting_date = frappe.utils.getdate()
			s.posting_time = frappe.utils.get_time(frappe.utils.get_datetime())
			s.company = erpnext.get_default_company()
			s.purpose = "Material Issue"
			
			toner_item_name = frappe.db.get_value("ASPA Machine", self.machine, "toner_item")
			toner_item = frappe.get_doc("Item", toner_item_name)

			s.append("items", {
				"item_code": toner_item.item_code,
				"s_warehouse": toner_item.default_warehouse,
				"qty": 1,
				"conversion_factor": 1.0,
			})

			s.save()
			s.submit()
			frappe.db.commit()
		else:
			frappe.msgprint("Stock entry already created against this toner request")