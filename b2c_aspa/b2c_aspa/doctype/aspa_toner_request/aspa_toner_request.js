// Copyright (c) 2016, MN Technique and contributors
// For license information, please see license.txt

frappe.ui.form.on('ASPA Toner Request', {
	refresh: function(frm) {
		frm.add_custom_button(__('Unlink Call'), function(){			
			frappe.call({
				method: "b2c_aspa.api.unlink_call_from_toner_request",
				args: {
					toner_request: cur_frm.doc.name,
				},
				callback: function (r){
					cur_frm.refresh_fields();
					frappe.show_alert("Done");					
				}
			});  
		});
	}
});
