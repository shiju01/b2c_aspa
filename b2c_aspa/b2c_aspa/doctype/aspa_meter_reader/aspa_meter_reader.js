// Copyright (c) 2016, MN Technique and contributors
// For license information, please see license.txt

frappe.ui.form.on('ASPA Meter Reader', {
	refresh: function(frm) {
	},
	onload: function(frm) {
		cur_frm.set_query("meter_reader_type", function() {
			return {
				"filters": [
					["name", "in", ["Employee", "Supplier", "User"]]
				]
			}
		});	
	},
	meter_reader_type: function(frm) {
		cur_frm.set_value("meter_reader", "");
		cur_frm.set_value("meter_reader_name", "");
	},
	meter_reader: function(frm) {
		if (frm.doc.meter_reader_type == "Supplier") {
			frappe.db.get_value("Supplier", cur_frm.doc.meter_reader, "supplier_name", function(r) {
				cur_frm.set_value("meter_reader_name", r.supplier_name);
			});
		} else if (frm.doc.meter_reader_type == "User") {
			frappe.db.get_value("User", cur_frm.doc.meter_reader, "username", function(r) {
				cur_frm.set_value("meter_reader_name", r.username);
			});
		} else if (frm.doc.meter_reader_type == "Employee") {
			frappe.db.get_value("Employee", cur_frm.doc.meter_reader, "employee_name", function(r) {
				cur_frm.set_value("meter_reader_name", r.employee_name);
			});
		}
		cur_frm.refresh_fields();
	}
});
