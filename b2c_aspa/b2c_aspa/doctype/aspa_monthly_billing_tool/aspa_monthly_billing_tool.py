# -*- coding: utf-8 -*-
# Copyright (c) 2015, MN Technique and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import json
from b2c_aspa.api import add_meter_reading


class ASPAMonthlyBillingTool(Document):

	def get_monthly_records(self):
		#ACTIVATE THIS
		#refdate = frappe.utils.datetime.datetime.strptime("01 {0} 2017".format(self.month), "%d %B %Y")

		refdate = frappe.utils.datetime.datetime(2017,04,1)

		first_of_lastmonth = frappe.utils.add_months(refdate, -1).replace(day=1)
		last_of_thismonth = frappe.utils.add_days(frappe.utils.add_months(refdate.replace(day=1), 1), -1)

		# print first_of_lastmonth, last_of_thismonth
		# print "READER", self.patch_meter_reader
		# print "Machines for reader", machines_for_reader

		machines_for_reader = frappe.get_all("ASPA Machine", filters=[["meter_reader", "=", self.patch_meter_reader]], fields=["*"])

		all_meters = frappe.get_all("ASPA Patch Meter")

		meters_for_all_machines = frappe.get_all("ASPA Machine Meter", fields=["*"])

		readings = frappe.db.sql("""SELECT machine, reading_time, meter_reader, reading, meter FROM `tabASPA Meter Reading`
			WHERE reading_type='Patch'
			AND DATE(reading_time) BETWEEN '{startdate}' AND '{enddate}' ORDER BY meter, reading_time;""".format(startdate=first_of_lastmonth, enddate=last_of_thismonth),
		as_dict=1)

		machines_info = []
		for machine in machines_for_reader:
			machine_meters = [m.patch_meter for m in meters_for_all_machines if m.parent == machine.name]
			machine_meter_readings = [r for r in readings if r.machine == machine.name and r.meter in machine_meters]

			readings_by_meter = []
			for meter in machine_meters:
				previous_reading_list = [r.reading for r in machine_meter_readings if
									r.meter == meter and
									(r.reading_time.year == first_of_lastmonth.year and r.reading_time.month == first_of_lastmonth.month)]
				previous_reading = previous_reading_list[0] if len(previous_reading_list) > 0 else 0

				current_reading_list = [r.reading for r in machine_meter_readings if
									r.meter == meter and
									(r.reading_time.year == last_of_thismonth.year and r.reading_time.month == last_of_thismonth.month)]
				current_reading = current_reading_list[0] if len(current_reading_list) > 0 else 0

				reading_by_meter = {
					"meter": meter,
					"previous_reading": previous_reading,
					"current_reading": current_reading
				}
				readings_by_meter.append(reading_by_meter)

			machine_row = frappe._dict({
				"mi": machine,
				"meters": machine_meters,
				"readings_by_meter" : readings_by_meter
			})
			machines_info.append(machine_row)

		return { "all_meters": all_meters, "machines_info": machines_info }

	def generate_fsma_invoices(self):
		abbr = frappe.db.get_value("Company", frappe.defaults.get_defaults().company, "abbr")

		refdate = frappe.utils.datetime.datetime(2017,03,31)

		first_of_lastmonth = frappe.utils.add_months(refdate, -1).replace(day=1)
		last_of_thismonth = frappe.utils.add_days(frappe.utils.add_months(refdate.replace(day=1), 1), -1)

		# first_of_lastmonth = frappe.utils.add_months(frappe.utils.datetime.date.today(), -1).replace(day=1)
		# last_of_thismonth = frappe.utils.add_days(frappe.utils.add_months(frappe.utils.datetime.date.today().replace(day=1), 1), -1)

		print first_of_lastmonth
		print last_of_thismonth

		machines = frappe.get_all("ASPA Machine", [["meter_reader", "!=", ""]])

		for machine in machines:
			#Results are sorted. Can look up row 0 and 1.
			current_last_readings = frappe.db.sql("""SELECT machine, reading_time, meter_reader, reading, meter FROM `tabASPA Meter Reading`
				WHERE reading_type='Patch'
				AND machine='{machine}'
				AND DATE(reading_time) BETWEEN '{startdate}' AND '{enddate}' ORDER BY meter, reading_time;""".format(machine=machine.name, startdate=first_of_lastmonth, enddate=last_of_thismonth),
			as_dict=1)

			si = frappe.new_doc("Sales Invoice")
			si.transaction_date = frappe.utils.get_datetime()
			si.due_date = frappe.utils.add_days(frappe.utils.get_datetime(), 10) #Might be decided via agreement.
			si.price_list = "Standard Selling"
			si.customer = frappe.db.get_value("ASPA Machine", machine.name, "customer")

			machine_meters_list = frappe.get_all("ASPA Machine Meter", filters={"parent": machine.name}, fields=["patch_meter"])
			machine_meters = [m.patch_meter for m in machine_meters_list]

			print "Machine", machine

			for meter in machine_meters:
				print meter

				readings_for_meter = [r for r in current_last_readings if r.meter == meter]
				# print "Current, last readings", current_last_readings
				# print "Readings for meter", readings_for_meter

				if len(readings_for_meter) != 2:
					frappe.throw("FSMA Invoicing: Some readings missing! Meter: {0}, Machine: {1}".format(meter, machine.name))

				last_meter_reading = readings_for_meter[0].reading
				current_meter_reading = readings_for_meter[1].reading
				difference = last_meter_reading - current_meter_reading
				adjusted_difference = int(difference * 0.99)

				item_description = "<div><table> <tr> <td>Last Meter Reading</td> <td>{last_meter_reading}</td> </tr> <tr> <td>Current Meter Reading</td> <td>{current_meter_reading}</td> </tr> <tr> <td>Difference</td> <td>{difference}</td> </tr> <tr> <td>Adjusted (x0.99)</td> <td>{adjusted_difference}</td> </tr> </table> </div>".format(last_meter_reading=last_meter_reading or 0.0, current_meter_reading=current_meter_reading or 0.0, difference=difference or 0.0, adjusted_difference=adjusted_difference or 0.0)

				si.append("items", {
					"item_name": meter,
					"uom": "Nos",
					"description": item_description,
					"qty": adjusted_difference,
					"rate": 555.0,
					"amount": adjusted_difference * 555.0,
					"conversion_factor": 1,
					"income_account": "Sales - " + abbr
				})

			si.save()
			frappe.db.commit()

	def save(self):
		print self

		# for x in xrange(1,10):
		# 	print "onsave"

		# if not readings:
		# 	frappe.throw("Readings not found")

		# try:
		# 	readings = json.loads(readings)
		# except Exception as e:
		# 	frappe.throw("Input values in readings are malformed.")

		# for mr in machine_readings:

