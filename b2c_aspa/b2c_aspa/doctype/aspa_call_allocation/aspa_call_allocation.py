# -*- coding: utf-8 -*-
# Copyright (c) 2015, MN Technique and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class ASPACallAllocation(Document):
	def validate(self):
		self.has_duplicates()
		call_workflow_state = frappe.db.get_value("ASPA Call", self.allocated_call, "workflow_state")

		if call_workflow_state in ["Logged", "Broken"]:
			self.change_call_workflow_state("Allocated")

	def change_call_workflow_state(self, state="Allocated"):
		allocated_call = frappe.get_doc("ASPA Call", self.allocated_call)
		allocated_call.workflow_state = state
		allocated_call.save()
		frappe.db.commit()

	def has_duplicates(self):
		if len(frappe.get_all(self.doctype, filters={"allocated_to": self.allocated_to, "allocated_call": self.allocated_call})) > 0:
			frappe.throw("Cannot allocate the same employee to this call again.")
