// Copyright (c) 2016, MN Technique and contributors
// For license information, please see license.txt

frappe.ui.form.on('ASPA Call', {
	onload: function(frm){
		$(frm.fields_dict['call_history_html'].wrapper)
					.html('<div class="text-muted text-center">Please select a machine.</div>');

	},
	refresh: function(frm) {
		frm.set_df_property("sb_visits", "hidden", (frm.doc.__islocal));
		frm.set_df_property("sb_allocations_html", "hidden", (frm.doc.__islocal));
		frm.set_df_property("btn_allocate", "hidden", 1);
		frm.set_df_property("call_status", "read_only", 1);

		render_visits(frm);
		if (frm.doc.machine) {
			render_call_history(frm); get_sss_remark(frm);
		}

		render_allocations(frm);

		frm.fields_dict.machine.new_doc = new_machine_quick_entry;
		frm.fields_dict.contact_person.new_doc = new_contact_quick_entry;
		cur_frm.set_query("contact_person", function() {
			return {
				"filters": {
					"customer": cur_frm.doc.customer
				}
			};
		});

		if (frm.doc.workflow_state == "Closed") {
			frm.set_df_property("call_status", "read_only", frm.doc.call_status);
			if (frm.doc.call_log_type == "Toner Log") {
				if (!frm.doc.toner_request) {
					frm.add_custom_button(__('Make Toner Request'), function(){

						frappe.db.get_value("ASPA Toner Request", {"call":self.name}, "name", function(r) {
							if(!r) {
								mnt.quick_entry("ASPA Toner Request",
									success = null,
									field_map = {
									"call": cur_frm.doc.name,
									"machine": cur_frm.doc.machine,
									"customer": cur_frm.doc.customer,
									"date_and_time": frappe.datetime.obj_to_user(frappe.datetime.now_datetime())
								});
							} else {
								frappe.msgprint("Toner request '"+ r.name +"' already exists for this call.");
							}
						});

					});
				}
			}

			if(!frm.doc.call_status) {
				frm.add_custom_button(__('Completed on Phone'), function(){
					cur_frm.set_value("call_status", "Completed on Phone");
				},__("Set Status"));
				frm.add_custom_button(__('Completed on Visit'), function(){
					cur_frm.set_value("call_status", "Completed on Visit");
				},__("Set Status"));
				frm.add_custom_button(__('Resolved by Customer'), function(){
					cur_frm.set_value("call_status", "Resolved by Customer");
				},__("Set Status"));
			}
			cur_frm.set_df_property("symptom", "hidden", 1);
			cur_frm.set_df_property("call_type", "hidden", 1);
		}



	},
	machine: function(frm) {
		if (frm.doc.machine) {
			fetch_and_set_info(frm);
			get_sss_remark(frm);
		} else {
			clear_machine_info(frm);
		}
		render_call_history(frm);
		render_readings_input_for_toner_log(frm);
	},
	customer: function(frm) {
		if (frm.doc.customer) {
			frappe.db.get_value("Contact", {"customer": frm.doc.customer, "is_primary_contact": 1}, "name", function(r) {
				frm.set_value("contact_person", r ? r.name : "");
			});
			frappe.db.get_value("Address", {"customer": frm.doc.customer, "is_primary_address": 1}, "name", function(r) {
				frm.set_value("customer_address", r ? r.name : "");
			});
			frappe.db.get_value("Customer", frm.doc.customer, "aspa_mcln", function(r) {
				frm.set_value("mcln", r ? r.aspa_mcln : "");
			});
		}
		render_call_history(frm);
	},
	contact_person: function(frm) {
		if (frm.doc.contact_person == "") {
			frm.set_value("contact_display", "");
			frm.set_value("contact_phone", "");
		}
		erpnext.utils.get_contact_details(frm);
	},
	customer_address: function(frm) {
		erpnext.utils.get_address_display(frm, "customer_address", "customer_address_display");
	},
	installation_address: function(frm) {
		erpnext.utils.get_address_display(frm, "installation_address", "installation_address_display");
	},
	after_save: function(frm){
		redirect_to_list(frm);
		//console.log("AFTER SUBMIT")
	},
	call_log_type: function(frm){
		render_call_history(frm);
		if(frm.doc.machine) {
			fetch_and_set_info(frm);
		} else {
			clear_machine_info(frm);
		}

		if (cur_frm.doc.call_log_type == "Toner Log") {
			cur_frm.set_df_property("symptom", "hidden", 1); cur_frm.set_value("symptom", "");
			cur_frm.set_df_property("call_type", "hidden", 1); cur_frm.set_value("call_type", "");
			render_readings_input_for_toner_log(frm);
		} else {
			cur_frm.set_df_property("symptom", "hidden", 0);
			cur_frm.set_df_property("call_type", "hidden", 0);
		}
	}
});


function redirect_to_list(frm) {
	if ((frm.doc.workflow_state == "Logged") || (frm.doc.workflow_state == "Allocated")) {
		frappe.route_options = {"workflow_state":["!=",["Closed"]]};
		frappe.set_route("List", "ASPA Call");
	}
}

function render_allocations(frm) {
	frappe.call({
		method: "frappe.client.get_list",
		args: {
			doctype: "ASPA Call Allocation",
			order_by: "creation DESC",
			fields: ["*"],
			filters: [
				["allocated_call", "=", frm.doc.name]
			]
		},
		callback: function(r){
			var wrapper_alloc = $(frm.fields_dict['call_allocations_html'].wrapper);
			wrapper_alloc.html(
				frappe.render_template("call_allocations", {
					"call_allocations": r.message,
					"show_log_visit_col": (
						cur_frm.doc.workflow_state == "Allocated" ||
						cur_frm.doc.workflow_state == "Re-allocation Pending" ||
						cur_frm.doc.workflow_state == "Broken"
					)
				}));
			wrapper_alloc.find(".log-visit").on("click", function(e) {
					add_visit($(this).attr("data-allocation"),
						cur_frm.doc.engineer,
						cur_frm.doc.engineer_name);
				})
			wrapper_alloc.find(".send-reminder").on("click", function(e) {
					send_reminder($(this).attr("data-allocation"),
						cur_frm.doc.engineer,
						cur_frm.doc.engineer_name);
				})
		}
	});
}

function add_visit(allocation, engineer, engineer_name) {
	frappe.call({
		method: "b2c_aspa.api.add_visit",
		args: { "allocation": allocation ,
				"engineer": engineer,
				"engineer_name": engineer_name
		},
		callback: function(r) {
			frappe.set_route("Form", "ASPA Call Visit", r.message);
		}
	});
}

function send_reminder(allocation, engineer, engineer_name) {
	frappe.call({
		method: "b2c_aspa.api.send_reminder",
		args: { "allocation": allocation,
				"engineer": engineer,
				"engineer_name": engineer_name
		},
		callback: function(r) {
			frappe.msgprint("Reminder sent to " + r.message)
		}
	});
}

function render_visits(frm) {
	frappe.call({
		method: "frappe.client.get_list",
		args: {
			doctype: "ASPA Call Visit",
			order_by: "creation DESC",
			fields: ["*"],
			filters: [
				["ASPA Call Visit", "allocated_call", "=", frm.doc.name]
			]
		},
		callback: function(r){
			$(frm.fields_dict['visits_html'].wrapper)
				.html(frappe.render_template("call_visits", {
					"call_visits": r.message,
					"show_log_visit_btn": (cur_frm.doc.workflow_state == "Allocated")
				}));
		}
	});
}

function render_call_history(frm) {
	frappe.call({
		method: "frappe.client.get_list",
		args: {
			doctype: "ASPA Call",
			order_by: "creation DESC",
			fields: ["*"],
			filters: [["machine", "=", frm.doc.machine], ["customer", "=", frm.doc.customer], ["name", "!=", frm.doc.name], ["call_log_type", "=", frm.doc.call_log_type]]
		},
		callback: function(r){
			if(r && cur_frm.doc.machine){
				$(frm.fields_dict['call_history_html'].wrapper)
					.html(frappe.render_template("call_history", {
						"call_history": r.message,
						"machine": frm.doc.machine,
						"call_log_type": frm.doc.call_log_type
					})).find(".view-all").on("click", function() {
						frappe.route_options = { "machine": cur_frm.doc.machine, "customer": cur_frm.doc.customer };
						frappe.set_route("List", "ASPA Call");
					});
			}
			if(!cur_frm.doc.machine){
				$(frm.fields_dict['call_history_html'].wrapper)
					.html('<div class="text-muted text-center">Please select a machine.</div>');
			}
			cur_frm.refresh_fields();
		}
	});
}

function fetch_and_set_info(frm) {
	frappe.call({
		method: "b2c_aspa.api.get_machine_info",
		args: { machine: frm.doc.machine },
		callback: function(r){
			cur_frm.set_value("customer", r.message.customer);
			cur_frm.set_value("machine_model", r.message.model);
			cur_frm.set_value("installation_date", r.message.installation_date);
			cur_frm.set_value("installation_address", r.message.installation_address);
			cur_frm.set_value("contract_type", r.message.contract_type);
			cur_frm.set_value("contract_end_date", r.message.contract_end_date);
			cur_frm.set_value("last_meter_reading", r.message.last_meter_reading_1);
			cur_frm.set_value("contact_person", r.message.contact_person);
			cur_frm.refresh_fields();
		}
	});
}

function clear_machine_info(frm) {
	console.log("Clearing info");
	frm.set_value("customer", "");
	frm.set_value("sss_remark", "");
	frm.set_value("machine_model", "");
	frm.set_value("customer_address", "");
	frm.set_value("contact_person", "");
	frm.fields_dict["customer_address_display"].$wrapper.html("");
	frm.set_value("contract_end_date", "");
	frm.set_value("last_meter_reading", "");
	frm.set_value("installation_date", "");
	frm.set_value("installation_address", "");
	frm.set_value("contract_type", "");
	frm.set_value("contract_end_date", "");
	frm.refresh_fields();
}

//Quick Entry

function new_contact_quick_entry() {
	frappe._from_link = this;
	mnt.quick_entry("Contact", null,
		{
			"first_name": "",
			"customer": cur_frm.doc.customer,
			"phone": this.$input.val()
		});
}

function new_machine_quick_entry() {
	frappe._from_link = this;
	mnt.quick_entry("ASPA Machine", null,
		{
			"serial_no": this.$input.val(),
		});
}
cur_frm.add_fetch("engineer", "employee_name", "engineer_name");

function get_sss_remark(frm){
	var frm = frm;
	frappe.model.with_doc("ASPA Machine", frm.doc.machine , function(){
		var m = frappe.model.get_doc("ASPA Machine", cur_frm.doc.machine);
		if(m.sss===1){
			cur_frm.set_value("sss_remark", m.remark_for_sss);
		}
	})
}

function render_readings_input_for_toner_log(frm) {
	if (cur_frm.doc.machine && cur_frm.doc.call_log_type == "Toner Log"){
		frappe.call({
			method: "get_meters_and_readings",
			doc: cur_frm.doc,
			callback: function(r){
				console.log("test",r);
				var wrapper = $(frm.fields_dict['meter_readings_input'].wrapper);
				wrapper.html(frappe.render_template("monthly_readings_with_input", {
						"readings_by_meter": r.message || []
					}));
				wrapper.append("<div class='row'><div class='col-xs-8'><button class='btn btn-default' id='btn-toner'>Save</button></div><div class='col-xs-8'>Current difference:</div></div>")
				console.log("btn", wrapper.find("#btn-toner"));
				//wrapper.append("<div class='panel panel-default'><table><tr>"{% for (var j=0; j < readings_by_meter.length; j+=1) { %}"<td><div>"{%= readings_by_meter[j].meter %}"</div><input class='input-with-feedback form-control input-sm ellipsis' placeholder="{%= readings_by_meter[j].meter %}" value="{%= readings_by_meter[j].previous_reading %}" disabled/></td>"{% } %}"</tr></table></div>")
				wrapper.find("#btn-toner").on("click", function(e) {
					var inputs = wrapper.find("[data-purpose='reading-input']");
					var readings_by_meter = []

					$.each(inputs, function(index, textbox) {
						reading_by_meter = {
							"meter": $(textbox).attr('data-meter'),
							"reading": $(textbox).val()
						};
						readings_by_meter.push(reading_by_meter);

					});
					console.log("TEST-", readings_by_meter);
					cur_frm.set_value('toner_readings' , JSON.stringify(readings_by_meter));
					/*frappe.call({
						method: "b2c_aspa.b2c_aspa.doctype.aspa_call.aspa_call.add_toner_call_readings",
						args: {
							"meter_reader": "ASPA-MRDR-00001",
							"machine": cur_frm.doc.machine,
							"readings": readings_by_meter,
							"source": cur_frm.doc.name
						},
						callback: function(r) {
							if (r && r.message) {
								frappe.show_alert(r.message, 5);
							}
						}
					});
*/
				});
			}
		});
	}
}
/*

var rows = $("[data-purpose='machine-data-row']");
				var machine_readings = [];

				$.each(rows, function(rowindex, row) {
					var machine_reading = {};

					machine_reading["machine"] = $(row).attr("id");


					machine_readingsding["current_readings"] = readings_by_meter;
					machine_readings.push(machine_reading);
				});

				frappe.call({
					method: "b2c_aspa.b2c_aspa.page.aspa_monthly_billing.aspa_monthly_billing.save_monthly_readings",
					args: {
						"patch_meter_reader": me.page.fields_dict['patch_meter_reader'].$input.val(),
						"month": me.page.fields_dict['month'].$input.val(),
						"monthly_readings": machine_readings
					},
					callback: function(r) {
						if (r && r.message) {
							frappe.show_alert(r.message, 5);
						}
					}
				});
			});*/