# -*- coding: utf-8 -*-
# Copyright (c) 2015, MN Technique and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
import json
from frappe.model.document import Document
from frappe.model.naming import make_autoname
from b2c_aspa.api import add_meter_reading

class ASPACall(Document):

	def autoname(self):
		if self.call_log_type == "Call Log":
			self.name = make_autoname("ASPA-CALL-" + '.MM.YY.-.#####')
		elif self.call_log_type == "Toner Log":
			self.name = make_autoname("ASPA-TONER-" + '.MM.YY.-.#####')

	def after_insert(self):
		if self.call_log_type == "Toner Log":
			add_toner_call_readings(frappe.session.user, self.machine ,self.toner_readings, self.name)

	def on_update_after_submit(self):
		if (self.call_log_type == "Call Log"):
			if self.workflow_state == "Closed":
				unsubmitted_visits = frappe.get_all("ASPA Call Visit", filters={"allocated_call": self.name, "docstatus": 0})
				if len(unsubmitted_visits) > 0:
					frappe.throw("Please submit all visits before completing the call. <br> Unsubmitted visit(s): <br>" \
						+ "<br>".join([v.name for v in unsubmitted_visits]))

				# all_visits = frappe.get_all("ASPA Call Visit", filters={"allocated_call": self.name, "docstatus": 1})
				# if len(all_visits) > 0:
				# 	self.call_status = "Completed on Visit"
				# else:
				# 	self.call_status = "Completed on Call"

	def before_update_after_submit(self):
		if self.workflow_state in ["Logged", "Re-allocation Pending"]:

			allocations = frappe.get_all("ASPA Call Allocation", filters={"allocated_call": self.name, "allocated_to": self.engineer}, fields=["name"])

			#Clicking Re-allocation pending fires an update_after_submit event (workflow statue is set)
			#The check will prevent a fatal error thrown by a duplicate assignment.
			if len(allocations) == 0:
				a = frappe.new_doc("ASPA Call Allocation")
				a.allocated_call = self.name
				a.allocated_to = self.engineer
				a.engineer_name = frappe.db.get_value("Employee", self.engineer, "employee_name")
				a.allocated_on = frappe.utils.datetime.datetime.today()
				a.notification_count = 1
				a.save()
				frappe.db.commit()

				self.workflow_state = "Allocated"

	def before_submit(self):
		if (self.call_log_type == "Toner Log") and (self.workflow_state != "Closed"):
			add_meter_reading(self.machine, "Call", frappe.utils.get_datetime(), self.current_meter_reading, "A4BW", frappe.session.user)
			self.workflow_state = "Closed"

	def get_meters_for_machine(self):
		meters_for_machine = frappe.get_all("ASPA Machine Meter", filters={"parent": self.machine}, fields=["patch_meter"])
		return meters_for_machine

	def get_meters_and_readings(self):
		refdate = frappe.utils.datetime.datetime.now()

		meters_for_all_machines = frappe.get_all("ASPA Machine Meter", fields=["*"])
		first_of_lastmonth = frappe.utils.add_months(refdate, -1).replace(day=1)
		last_of_thismonth = frappe.utils.add_days(frappe.utils.add_months(refdate.replace(day=1), 1), -1)

		readings = frappe.db.sql("""SELECT A.machine, A.reading_time, A.meter_reader, A.reading, A.meter
			FROM `tabASPA Meter Reading` AS A inner join `tabDynamic Link` AS B on
			A.name=B.parent WHERE machine = '{machine}' AND B.link_name = '{call_name}' ORDER BY meter, reading_time;"""
			.format(machine=self.machine, call_name= self.name),
		as_dict=1)
		# print "readings", readings
		machine_meters = [m.patch_meter for m in meters_for_all_machines if m.parent == self.machine]
		machine_meter_readings = [r for r in readings if r.machine == self.machine and r.meter in machine_meters]

		readings_by_meter = []
		for meter in machine_meters:

			previous_reading_list = [r.reading for r in machine_meter_readings if
								r.meter == meter and
								(r.reading_time.year == first_of_lastmonth.year and r.reading_time.month == first_of_lastmonth.month)]
			previous_reading = previous_reading_list[0] if len(previous_reading_list) > 0 else 0

			reading_list = [r.reading for r in machine_meter_readings if
								r.meter == meter]
			reading = reading_list[0] if len(reading_list) > 0 else 0
			print "machine_meter", reading
			reading_by_meter = {
				"meter": meter,
				"previous_reading": reading,
				"current_reading": 0
			}
			readings_by_meter.append(reading_by_meter)
			# print "test2", readings_by_meter
		return readings_by_meter

@frappe.whitelist()
def add_toner_call_readings(meter_reader, machine ,readings, source):
	#	print "Called"
	# previous_readings = get_monthly_records(patch_meter_reader, month)
	# previous_readings = previous_readings["machines_info"]

	# print "CURRENT READINGS: ", readings
	# # print "PREVIOUS READINGS: ", previous_readings
	# for x in xrange(1,10):
	# 	print "field", readings
	# 	print "add_toner source", source

	if readings:
		readings = json.loads(readings)

	meter_reader= frappe.db.get_value("ASPA Meter Reader", {"meter_reader":meter_reader}, 'name')

	for reading in readings:
		add_meter_reading(machine,
			"Toner Call",
			frappe.utils.get_datetime(),
			reading.get("reading"),
			reading.get("meter"),
		 	meter_reader, source=source)


