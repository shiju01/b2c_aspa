// Copyright (c) 2016, MN Technique and contributors
// For license information, please see license.txt

frappe.ui.form.on('ASPA Machine', {
	refresh: function(frm) {
		//Filter contact persons by customer
		frm.set_query("contact_person", function() {
			return {
				"filters": {
					"customer": frm.doc.customer
				}
			};
		});

		frm.set_query("active_agreement", function() {
			return {
				"filters": {
					"machine": cur_frm.doc.name
				}
			}
		});
		//Filter contact persons by customer
		frm.set_query("installation_address", function() {
			return {
				"filters": {
					"customer": frm.doc.customer
				}
			};
		});
	},
	customer: function(frm) {
		frappe.db.get_value("Contact", {"customer":frm.doc.customer}, "name", function(r) {
			if((r) && (r.name)) {
				frm.set_value("contact_person", r.name);
			} else {
				frm.set_value("contact_person", "");
			}
		});

		frappe.db.get_value("Address", {"customer":frm.doc.customer}, "name", function(r) {
			if((r) && (r.name)) {
				frm.set_value("installation_address", r.name);
			} else {
				frm.set_value("installation_address", "");
			}
		});		
	},
	installation_address: function(frm) {
		erpnext.utils.get_address_display(frm, "installation_address", "address_display");
	},
});
