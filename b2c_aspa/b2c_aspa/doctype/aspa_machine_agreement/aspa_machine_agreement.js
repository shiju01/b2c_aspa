// Copyright (c) 2016, MN Technique and contributors
// For license information, please see license.txt

frappe.ui.form.on('ASPA Machine Agreement', {
	refresh: function(frm) {
		frm.add_custom_button(__("Make"), function() {
			frappe.msgprint("Done")
		}, __("Make Payment"));
		frm.add_custom_button(__("View"), function() {
			frappe.msgprint("Done")
		}, __("Outstanding"));
		frm.add_custom_button(__("View"), function() {
			frappe.msgprint("Done")
		}, __("Machine History"));

	}
});
