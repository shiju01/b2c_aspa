# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "b2c_aspa"
app_title = "B2C ASPA"
app_publisher = "MN Technique"
app_description = "Authorized Service Provider Assistant"
app_icon = "octicon octicon-rocket"
app_color = "#ffbc00"
app_email = "support@mntechnique.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/b2c_aspa/css/b2c_aspa.css"
app_include_js = "/assets/js/aspa.min.js"

# include js, css files in header of web template
# web_include_css = "/assets/b2c_aspa/css/b2c_aspa.css"
# web_include_js = "/assets/b2c_aspa/js/b2c_aspa.js"

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "b2c_aspa.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "b2c_aspa.install.before_install"
# after_install = "b2c_aspa.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "b2c_aspa.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"ToDo": {
# 		"validate": "b2c_aspa.api.todo_validate"
# 	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"b2c_aspa.tasks.all"
# 	],
# 	"daily": [
# 		"b2c_aspa.tasks.daily"
# 	],
# 	"hourly": [
# 		"b2c_aspa.tasks.hourly"
# 	],
# 	"weekly": [
# 		"b2c_aspa.tasks.weekly"
# 	]
# 	"monthly": [
# 		"b2c_aspa.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "b2c_aspa.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "b2c_aspa.event.get_events"
# }

fixtures = [
				{
					"dt": "Property Setter", "filters": [["name", "in",
						["Contact-search_fields"]
					]]
				},
				# {
				# 	"dt": "Workflow", "filters": [["name", "in",
				# 		["ASPA Call Workflow"]
				# 	]]
				# },
				# {
				# 	"dt": "Workflow State", "filters": [["name", "in",
				# 		[
				# 		"Open",
				# 		"Logged",
				# 		"Broken",
				# 		"Allocated",
				# 		"Closed",
				# 		"Abandoned",
				# 		"Re-allocation Pending"
				# 		]
				# 	]]
				# },
				# {
				# 	"dt": "Workflow Action", "filters": [["name", "in",
				# 		[
				# 		"Log Call",
				# 		"Break Call",
				# 		"Abandon Call",
				# 		"Close Call",
				# 		"Re-allocate Call"
				# 		]
				# 	]]
				# },
				{
					"dt": "Print Format", "filters": [["name", "in",
						[
							"ASPA FSMA Invoice"
						]
					]]
				},
				# {
				# 	"dt": "Custom Field", "filters": [["name", "in",
				# 		[
				# 			"Customer-aspa_mcln",
				# 			"Customer-aspa_customer_id"
				# 		]
				# 	]]
				# },
				"Custom Script",
				{
					"dt": "Custom Field", "filters": [["name", "in",
						[
							"Customer-aspa_mcln",
							"Customer-aspa_customer_id",
							"Customer-customer_mvat_no",
							"Sales Invoice-aspa_customer_id",
							"Sales Invoice-customer_tel_no",
							"Sales Invoice-aspa_machine",
							"Sales Invoice-aspa_machine_model_no",
							"Sales Invoice-aspa_machine_agreement_start_date",
							"Sales Invoice-aspa_machine_agreement"
						]
					]]
				}
			]