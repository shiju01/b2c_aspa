from __future__ import unicode_literals
import frappe
import datetime
from frappe import _


@frappe.whitelist()
def add_meter_reading(machine, reading_type, reading_time, reading, meter, meter_reader, remark=None, source= None):
	try:
		mr = frappe.new_doc("ASPA Meter Reading")
		mr.machine = machine
		mr.reading_type = reading_type or ""
		mr.reading_time = reading_time
		mr.reading = reading
		mr.meter = meter
		mr.meter_reader = meter_reader
		mr.remark = remark
		source_map = {"Toner Call": "ASPA Call", "Visit": "ASPA Call Visit"}
		if reading_type in source_map:
			mr.append("Links",{
				"link_doctype": source_map[reading_type],
				"link_name": source
				})
		mr.insert()
		frappe.db.commit()
	except Exception as e:
		raise

@frappe.whitelist()
def get_readings(machine=None, reading_type=None, meter_reader=None, meters=None, source=None, from_date=None, to_date=None):
	filters = []

	if machine:
		filters.append(" machine ='" + machine + "' ")

	if reading_type:
		filters.append(" reading_type = '" + reading_type + "' ")

	if meter_reader:
		filters.append(" meter_reader = '" + meter_reader + "' ")

	if meters:
		filters.append(" meter in (" + ",".join(["'{0}'".format(meter) for meter in meters]) + ") ")

	if from_date and to_date:
		filters.append(" (DATE(reading_time) BETWEEN '{startdate}' AND '{enddate}') ".format(startdate=from_date, enddate=to_date))

	filters_string = ""
	if len(filters) > 0:
		filters_string = " WHERE {0}".format(" AND ".join(filters))

	sql = """SELECT a.*, b.link_name 
	 	FROM `tabASPA Meter Reading` AS a LEFT OUTER JOIN `tabDynamic Link` AS b 
	 	ON a.name = b.parent 
	 	{filters}
	 	ORDER BY a.meter, a.reading_time DESC;""".format(filters=filters_string)

	print "SQL", sql
	
	readings = frappe.db.sql(sql, as_dict=1)

	return readings

@frappe.whitelist()
def get_readings_for_toner_call(machine, meter_reader, toner_call=None):
	readings = get_readings(machine=machine)
	
	return [readings[0]] #Latest reading for machine.

'''
	Readings by meter reader, month
'''
@frappe.whitelist()
def get_readings_for_monthly_billing(meter_reader, month, year):
	refdate = frappe.utils.datetime.datetime.strptime("01 {0} {1}".format(month, year), "%d %B %Y")
	first_of_lastmonth = frappe.utils.add_months(refdate, -1).replace(day=1)
	last_of_thismonth = frappe.utils.add_days(frappe.utils.add_months(refdate.replace(day=1), 1), -1)

	readings = get_readings(meter_reader=meter_reader, reading_type="Patch", from_date=first_of_lastmonth, to_date=last_of_thismonth)

	all_meters = frappe.get_all("ASPA Patch Meter")

	machines_for_reader = frappe.get_all("ASPA Machine", filters=[["meter_reader", "=", meter_reader]], fields=["*"])
	meters_for_all_machines = frappe.get_all("ASPA Machine Meter", fields=["*"])
	
	machines_info = []
	for machine in machines_for_reader:
		machine_meters = [m.patch_meter for m in meters_for_all_machines if m.parent == machine.name]
		machine_meter_readings = [r for r in readings if r.machine == machine.name and r.meter in machine_meters]
			
		readings_by_meter = []
		for meter in machine_meters:
			readings_prev = [{"reading": r.reading, "reading_time": r.reading_time} for r in machine_meter_readings 
								if (r.meter = meter) 
								and (r.reading_time.year == first_of_lastmonth.year) and (r.reading_time.month == first_of_lastmonth.month)
							]
			previous_reading = readings_prev[0] if len(readings_prev) > 0 else {"reading": 0, "reading_time": None}

			readings_current = [{"reading": r.reading, "reading_time": r.reading_time} for r in machine_meter_readings 
								if (r.meter = meter) 
								and (r.reading_time.year == last_of_thismonth.year) and (r.reading_time.month == last_of_thismonth.month)
							]
			current_reading = readings_current[0] if len(readings_current) > 0 else {"reading": 0, "reading_time": None}

			reading_by_meter = {
				"meter": meter,
				"previous_reading": previous_reading.get("reading"),
				"previous_reading_time": previous_reading.get("reading_time"),
				"current_reading": current_reading.get("reading"),
				"current_reading_time": current_reading.get("reading_time")
			}
			readings_by_meter.append(reading_by_meter)
		
		machine_info = {"mi": machine, "meters": machine_meters, "readings_by_meter":readings_by_meter }
		machines_info.append(machine_info)

	return { "all_meters": all_meters, "machines_info": machines_info }
